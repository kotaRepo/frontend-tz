import './App.css';

import SearchPage from './components/SearchPage/index.js';

function App() {
  return (
    <div className="App">
      <SearchPage />
    </div>
  );
}

export default App;
