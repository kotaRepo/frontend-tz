import React from 'react';
import './style.css'

const TimeZones = ({
  timeZones=[],
  onClickZone,
}) => {
  return (
    <>
    <div className="timezonelist">
    { timeZones.map((data,index) => {
      return (
      <div
        className="zoneItem"
        onClick={(e) => onClickZone(e, data.name)}>
          {data.name_to_display}
      </div>
          )
    })}
    </div>
    </>
  );
}

export default TimeZones
