import React from 'react';
import './style.css';

const SearchBar = ({
    input,
    onChangeInput,
    onClickInput,
}) => {

const handleKeyword = (event, value) => {
    event.preventDefault();
    onChangeInput(value);
};
const handleOnClick = (event, value) => {
    event.preventDefault();
    onClickInput(value);
};

  return (
    <input 
     className= "search-input" 
     key="r1"
     value={input}
     placeholder={"Search Time Zone"}
     onChange={(e) => handleKeyword(e, e.target.value)}
     onClick={(e) => handleOnClick(e, e.target.innerText)}
    />
  );
}

export default SearchBar