import React, { useState, useEffect } from 'react';
import SearchBar from '../SearchBar/index.js';
import TimeZones from '../TimeZones/index.js';
import { BASE_PATH_SERVER } from '../../utils/constants';
import './style.css';

let arrayBoxes = [];

const SearchPage = (props) => {
  const [input, setInput] = useState('');
  const [timeZonesDefault, setTimeZonesDefault] = useState();
  const [timeZones, setTimeZones] = useState();
  const [showTimeZones, setShowTimeZones] = useState(false);
  const [timeZonesSelected, setTimeZonesSelected] = useState();
  
  //Get data first time use and save 
  const fetchDataFTU = async () => {
      return await fetch(`${BASE_PATH_SERVER}`)
      .then(response => response.json())
      .then(data => {
         setTimeZones(data.result) 
         setTimeZonesDefault(data.result)
       });
  }
  // Get current time zone and its local time
  const getTimeZone = async name => {

    //arrayBoxes.push(name);
    //setTimeZonesSelected(arrayBoxes);
    
  return await fetch(`${BASE_PATH_SERVER}/${name}`)
  .then(response => response.json())
  .then(data => {
    arrayBoxes.push(data);
    setTimeZonesSelected(arrayBoxes);
    setShowTimeZones(false);
   });
  }

  const updateInput = input => {
    if(timeZonesDefault) {
      const filtered = timeZonesDefault.filter(zones => {
        return zones.name.toLowerCase().includes(input.toLowerCase())
       })
       setInput(input);
       setTimeZones(filtered);
       setShowTimeZones(true);
    }
  }

  const updateTextInput = input => {
    /*setInput(input);
    setTimeZones([]);
    setShowTimeZones(true);*/
  }

  const removeBox = _id => {
    for(let i=0; i<arrayBoxes.length; i++){
      if (arrayBoxes[i]['time_zone']._id === _id){
        arrayBoxes[i]['time_zone'] = null;
      }
    }
  }

 const updateBoxes = (e, input) => {
    setInput(input);
    setTimeZones([]);
    setShowTimeZones(true);
    getTimeZone(input);
 }

  useEffect( () => {fetchDataFTU()},[]);
	
  return (
    <>
      <h1>Time Zones</h1>
      <div className="main">
      <div>
        <SearchBar 
        input={input} 
        onChangeInput={updateInput}
        onClickInput={updateTextInput}
        />
      </div>
      <div className="zones">
        {showTimeZones && (
          <TimeZones 
          onClickZone={updateBoxes}
          timeZones={timeZones}/>
        )}
      </div>
      <div className="container">
      
        {timeZonesSelected && timeZonesSelected.map((data,index) => {
            return (
              <div className="boxes">
              <button className="close" onClickBtn={removeBox}>X</button>
              <h3>{data['time_zone']['name_to_display']}</h3>
              <label>Local Time</label>
              <h3>{data['time_zone']['local_time']}</h3>
              </div>
            )
        })}
      </div>
      </div>
    </>
   );
}

export default SearchPage