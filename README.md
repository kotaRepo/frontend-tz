## Setup - Time Zone

- Prerequisites

    * [NodeJS](https://nodejs.org/en/) with [npm](https://www.npmjs.com/) v6+ or nvm install 8.3.11

## Instructions to run application    

* Clone repository
* Move to project root
* $ run npm install
* $ run npm start

*Server*

 -The app is serving locally in [http://localhost:3000]
 -The app is pointing to server locally in [http://localhost:5000]





